Author:Ameya Bhandari

Contact: ameyab@uoregon.edu

Description: Adding authentication services to project 6.

Stuff to address: The api's work fully on port 5001 but the homepage for port 5000 allow access to the API's. The buttons on the homepage of the port 5000 page expose whats stored in MongoDB. For the top k times, it works on the 5001 port but there's no button for it on my port 5000 page. For the /api/register, the id, username, and hashed password are stored in MondoDB also. Also just a quick side note, my csv's were done manually because I read on piazza that I should mention that. Also for the token API, it says page not found but it shows the token anyway so...

Lastly, my dockerfile is in the laptop folder.

Thank you for the term, and have a good and safe summer.