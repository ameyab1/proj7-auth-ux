"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
import flask
from flask import Flask, redirect, url_for, request, render_template, flash
from flask_restful import Resource, Api, reqparse
from pymongo import MongoClient
from forms import LoginForm
from passlib.apps import custom_app_context as pwd_context
import arrow  # Replacement for datetime, based on moment.js
import acp_times # Brevet time calculations
import config
import json
import logging
from itsdangerous import (TimedJSONWebSignatureSerializer \
                                  as Serializer, BadSignature, \
                                  SignatureExpired)
import time


###
# Globals
###
app = flask.Flask(__name__)
api = Api(app)
CONFIG = config.configuration()
#app.secret_key = CONFIG.SECRET_KEY
app.config['SECRET_KEY'] = 'the quick brown fox jumps over the lazy dog'
app.secret_key = app.config['SECRET_KEY']
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb
db2 = client.thru
###
# Pages
###



@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    _items = db.tododb.find()
    
    items = [item for item in _items]
    #app.logger.debug(items + "HELLO")
    return flask.render_template('calc.html', items=items)


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    
    open_time = acp_times.open_time(km, 200, '2013-05-05')
    close_time = acp_times.close_time(km, 200, '2013-05-05')
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)
'''
@app.route('/new', methods=['POST'])
def new():
    item_doc = {
        'km': request.form['km'],
        'distance': request.form['distance']
    }
    db.tododb.insert_one(item_doc)

    return redirect(url_for('_calc_times'))
'''
@app.route('/new', methods=['POST'])
def new():
    for x in range(len(request.form.getlist('km'))):
        if request.form.getlist('km')[x] != "":
            item_doc = {
                #'km': request.form.getlist('km')[x],
                #'distance': request.form['distance'],
                'open': request.form.getlist('open')[x],
                'close': request.form.getlist('close')[x]
            }
            db.tododb.insert_one(item_doc)

    return redirect(url_for('index'))

@app.route('/newer')
def trial():
    _items = db.tododb.find()
    items = [item for item in _items]
    return render_template('displayer.html', items=items)



class Listall(Resource):
    def get(self):
        
        _items = db.tododb.find()
        items = [item for item in _items]
        collector = {}
        for item in items:
            app.logger.debug(item['open']+item['close'])
            #collector.append({'open': item['open']})
            #collector['open'].append(item['open'])
            #collector['close'].append(item['close'])
            #collector.append({'close': item['close']})
        app.logger.debug(collector)
        json_items = json.dumps(items, default=str)
        app.logger.debug(json_items)
        return json_items
    '''
        return {
            'Laptops': ['Mac OS', 'Dell', 
            'Windozzee',
	    'Yet another laptop!',
	    'Yet yet another laptop!'
            ]
        } 
'''
api.add_resource(Listall, '/listAll')

class Listopenonly(Resource):
    def get(self):
        
        _items = db.tododb.find()
        items = [item['open'] for item in _items]
        json_items = json.dumps(items, default=str)
        app.logger.debug(json_items)
        return json_items
api.add_resource(Listopenonly, '/listOpenOnly')



class Listcloseonly(Resource):
    def get(self):
        
        _items = db.tododb.find()
        items = [item['close'] for item in _items]
        json_items = json.dumps(items, default=str)
        app.logger.debug(json_items)
        return json_items
api.add_resource(Listcloseonly, '/listCloseOnly')

class Listallcsv(Resource):
    def get(self):
        
        _items = db.tododb.find()
        items = [item for item in _items]
        collector = []
        counter=0
        for item in items:
            app.logger.debug(item['open']+item['close'])
            collector.append(item['open'] +", " + item['close'])
            counter+=1
            if len(items)<counter:
                collector.append(",")
            #collector['open'].append(item['open'])
            #collector['close'].append(item['close'])
            #collector.append({'close': item['close']})
        app.logger.debug(collector)
        #json_items = json.dumps(items, default=str)
        #app.logger.debug(json_items)
        return collector
api.add_resource(Listallcsv, '/listAll/csv')


class Listopenonlycsv(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('top',type=int)
        app.logger.debug(parser.parse_args()['top'])
        _items = db.tododb.find()
        items = [item for item in _items]
        collector = []
        counter=0
        for item in items:
            
            collector.append(item['open'])
            counter+=1
            if counter == parser.parse_args()['top']:
                break
            if len(items)<counter:
                collector.append(",")
            
        return collector
api.add_resource(Listopenonlycsv, '/listOpenOnly/csv', endpoint='listOpenOnly/csv')

class Listcloseonlycsv(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('top',type=int)
        _items = db.tododb.find()
        items = [item for item in _items]
        collector = []
        counter=0
        for item in items:
            
            collector.append(item['close'])
            counter+=1
            if counter == parser.parse_args()['top']:
                break
            if len(items)<counter:
                collector.append(",")
            
        return collector
api.add_resource(Listcloseonlycsv, '/listCloseOnly/csv', endpoint='listCloseOnly/csv')

class Listalljson(Resource):
    def get(self):
        
        _items = db.tododb.find()
        items = [item for item in _items]
        json_items = json.dumps(items, default=str) 
        return json_items
api.add_resource(Listalljson, '/listAll/json')

class Listopenonlyjson(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('top',type=int)
        _items = db.tododb.find()
        items=[]
        counter=0
        app.logger.debug(parser.parse_args()['top'])
        #for i in range(parser.parse_args()['top']):
        if parser.parse_args()['top'] != None:
            #for i in range(parser.parse_args()['top']):
            for item in _items:
                app.logger.debug(item['open'])
                items.append(item['open'])
                counter+=1
                if counter == parser.parse_args()['top']:
                    break
                    #items[i]=[_items[i]['open']]
                
        else:
            items = [item['open'] for item in _items]

        json_items = json.dumps(items, default=str)
        app.logger.debug(json_items)
        return json_items
        #return 
api.add_resource(Listopenonlyjson, '/listOpenOnly/json', endpoint='listOpenOnly/json')



class Listcloseonlyjson(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('top',type=int)
        _items = db.tododb.find()
        items=[]
        counter=0
        if parser.parse_args()['top'] != None:
            #for i in range(parser.parse_args()['top']):
            for item in _items:
                
                items.append(item['close'])
                counter+=1
                if counter == parser.parse_args()['top']:
                    break                
        else:
            items = [item['close'] for item in _items]


        #items = [item['close'] for item in _items]
        json_items = json.dumps(items, default=str)
        app.logger.debug(json_items)
        return json_items
api.add_resource(Listcloseonlyjson, '/listCloseOnly/json', endpoint='listCloseOnly/json')

def hash_password(password):
    return pwd_context.encrypt(password)

def verify_password(password, hashVal):
    return pwd_context.verify(password, hashVal)



@app.route('/api/register', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        flash('Login requested for user {}, remember_me={}'.format(
            form.username.data, form.remember_me.data))
        hVal = hash_password(form.password.data)
        item_doc={'username': form.username.data,
                  'password': hVal}
        db2.thru.insert_one(item_doc)
        #app.logger.debug(db2.thru.find())
        #app.logger.debug(hVal)
        
        if verify_password(form.password.data, hVal):
            status_code = flask.Response(status=201)
            app.logger.debug("Success")
            return status_code
        else:
            status_code = flask.Response(status=400)
            app.logger.debug("Failure")
            return status_code
        return redirect(url_for('index'))
    return render_template('login.html',  title='Sign In', form=form)
'''
class Loginer(Resource):
    
    def get(self):
        form = LoginForm()
        if form.validate_on_submit():
            flash('Login requested for user {}, remember_me={}'.format(
                form.username.data, form.remember_me.data))
            hVal = hash_password(form.password.data)
            item_doc={'username': form.username.data,
                      'password': hVal}
            db2.thru.insert_one(item_doc)
            #app.logger.debug(db2.thru.find())
            #app.logger.debug(hVal)
        #return render_template('login.html',  title='Sign In', form=form)
    
    def post(self):
        if form.validate_on_submit():
            if verify_password(form.password.data, hVal):
                status_code = flask.Response(status=201)
                app.logger.debug("Success")
                return status_code
            else:
                status_code = flask.Response(status=400)
                app.logger.debug("Failure")
                return status_code
api.add_resource(Loginer, '/api/register')
'''

#@login_manager.request_loader
#def load_user_from_request(request):
def generate_auth_token(expiration=600):
        
    
    s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
    #s = Serializer('test1234@#$', expires_in=expiration)
    # pass index of user
    #return s.dumps({'id': 1})
    return s.dumps({'id': 1})

                        
def verify_auth_token(token):
    s = Serializer(app.config['SECRET_KEY'])
    #s = Serializer('test1234@#$')
    try:
        data = s.loads(token)
    except SignatureExpired:
        return None    # valid token, but expired
    except BadSignature:
        return None    # invalid token
    return "Success"

#@app.route('/api/token', methods=['GET'])
class Token(Resource):
    def get(self):
        #form = LoginForm()
        #if verify_password(form.password.data, hVal):
        t = generate_auth_token(10)
        #for i in range(1, 20):
        if verify_auth_token(t) == "Success":
            string = json.dumps(t.decode('utf-8'))
            return string
        else:
            status_code = flask.Response(status=401)
            app.logger.debug("Failure")
            return status_code
        #app.logger.debug("Success")
        #return status_code
        #else:
            #status_code = flask.Response(status=401)
            #app.logger.debug("Failure")
            #return status_code
api.add_resource(Token, '/api/token')
    

#############
'''
app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)
'''
if __name__ == "__main__":
    #print("Opening for global access on port {}".format(CONFIG.PORT))
    #app.run(port=CONFIG.PORT, host="0.0.0.0")
    app.run(host='0.0.0.0', debug=True)
