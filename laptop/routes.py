from flask import Flask
from flask import render_template, flash, redirect, url_for
from forms import LoginForm
from config import Config
from passlib.apps import custom_app_context as pwd_context
import logging

app = Flask(__name__)
app.config.from_object(Config)

@app.route('/')
@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        flash('Login requested for user {}, remember_me={}'.format(
            form.username.data, form.remember_me.data))
        app.logger.debug(form.password.data)
        return redirect(url_for('index'))
    return render_template('login.html',  title='Sign In', form=form)


@app.route('/index')
def index():
    user = {'username': 'Roam'}
    posts = [
        {
            'author': {'username': 'A'},
            'body': 'Beautiful day in Eugene!'
        },
        {
            'author': {'username': 'B'},
            'body': 'The Avengers movie was so cool!'
        }
    ]
    return render_template('index.html', title='Home', user=user, posts=posts)

def hash_password(password):
    return pwd_context.encrypt(password)

def verify_password(password, hashVal):
    return pwd_context.verify(password, hashVal)


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
